#include <Chrono.h>

// Connexions :
// - Baguettes sur la masse
// - Récepteur d'amulette sur digital 3 et digital 4
// (ou inversement : baguette sur D3, récepteur sur masse)


// Un tintement ferme un circuit.
int pinCloche1 = A0;
int pinCloche2 = A1;

// Alors on premettra la danse des dragons quelques temps
// commandés par des relais.
int pinDanseur1 = 7;
int pinDanseur2 = 8;

// Combien de temps dansent-ils ?
unsigned long tempsDeDanse = 30;

// 2 Chronomètres
Chrono ChronoDanseur1 = Chrono();
Chrono ChronoDanseur2 = Chrono();


void setup() {
  // internal pullup (simplifier le circuit externe)
  // attention : lecture inversée : LOW = circuit fermé
  pinMode(pinCloche1, INPUT);
  pinMode(pinCloche2, INPUT);

  pinMode(pinDanseur1, OUTPUT);
  pinMode(pinDanseur2, OUTPUT);
  
  ChronoDanseur1.stop();
  ChronoDanseur2.stop();
  
  //Serial.begin(115200);
}

void loop() {
  //Serial.println(analogRead(pinCloche1));

  // Danseur 1
  if (ChronoDanseur1.ready()) {
    if (analogRead(pinCloche1) >= 900) {
      ChronoDanseur1.start();
      digitalWrite(pinDanseur1, HIGH);
    }
  } else {
    if (ChronoDanseur1.get() >= tempsDeDanse * 1000) {
      ChronoDanseur1.stop();
      digitalWrite(pinDanseur1, LOW);
    }
  }
  
  // Danseur 2 (même code)
  if (ChronoDanseur2.ready()) {
    if (analogRead(pinCloche2) >= 900) {
      ChronoDanseur2.start();
      digitalWrite(pinDanseur2, HIGH);
    }
  } else {
    if (ChronoDanseur2.get() >= tempsDeDanse * 1000) {
      ChronoDanseur2.stop();
      digitalWrite(pinDanseur2, LOW);
    }
  }

}
