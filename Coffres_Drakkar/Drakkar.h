#ifndef Drakkar_h
#define Drakkar_h

#include "Arduino.h"
#include "Color.h"
#include "LedRGB.h"
#include "BarriereIR.h"
#include <Servo.h>

// Juste un conteneur de fonctions 
class Drakkar {
  public: 
    Drakkar(int pinServo, unsigned long delaiAttente, LedRGB ledsYeux, Color couleurYeuxAllumes, int pinCapteurBoule, int pinSignalSonore);
    
    Servo servoLacheurBoule;
    
    int pinServoLacheurBoule;
    long angleBouleRetenue;
    long angleBouleLachee;
    int vitesseOuverture;
    unsigned long delaiBouleOuvert;
    unsigned long delaiAvantLacherBoule;
    
    LedRGB ledsYeux;
    Color couleurYeuxAllumes;
    Color couleurYeuxEteints;
    
    int pinCapteurBoule;
    int pinSignalSonore;
        
    void initialiser();

    bool boulePresente();
    
    void allumerYeux();
    void eteindreYeux();
    
    void retenirBoule();
    void tenirBoule();
    void lacherBoule();
    void envoyerBoule();
    void angleTenueBoule(long angle);
    
    void envoyerSon();
    
};


#endif



