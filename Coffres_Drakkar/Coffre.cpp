#include "Arduino.h"
#include "Coffre.h"
#include "Button.h"
#include "DmxSimple.h"
#include "Color.h"
#include "Chrono.h"


// Contructeur
// On affecte les pin et on crée les boutons
Coffre::Coffre(int pinD, int pinO, int pinV, int pinL, int pinS, DmxSimpleClass &DmxSimple, int DmxAdresse, Color couleur) :
  btnDeclencheur(pinD, BUTTON_PULLUP_INTERNAL),
  btnOuverture(pinO, BUTTON_PULLUP_INTERNAL),
  pinTestDeclencheur(pinD),
  pinTestOuverture(pinO),
  pinVerrouillage(pinV),
  pinLeds(pinL),
  pinSon(pinS),
  DmxSimple(DmxSimple),
  DmxAdresse(DmxAdresse),
  couleur(couleur)
  
{
  pinMode(pinVerrouillage,      OUTPUT);
  pinMode(pinLeds,              OUTPUT);

  actif = false;
  
  delaiPourVerrouillage = 1000; // 1 seconde

  // le coffre s'active (indépendamment de l'ouverture / fermeture)
  // lorsque tous les coffres sont actifs, une action sera réalisée.
  desactiver();
};

// Initialitiaon
// On éteint tout et on verrouille le coffre s'il est fermé
void Coffre::initialiser() {
  
  eteindreLeds();
  eteindreProjecteur();
  couperSon();
  desactiver();
  chronoVerrouillage.stop();
  verrouiller();
  
  if (estOuvert()) {
    deverrouiller(); 
  }
  
};


// Test de l'état du coffre
void Coffre::actualiser() {

  // Coffre fermé
  if (estFerme()) {
    if (declencheurPresent()) {
      deverrouiller();
    } else {
      // verrouiller après un certain temps
      if (estDeverrouille()) {
        if (chronoVerrouillage.ready()) {
          chronoVerrouillage.start(); 
        } else if (chronoVerrouillage.get() >= delaiPourVerrouillage) {
          verrouiller();
          chronoVerrouillage.stop();
        }
      }
    }
  } 
  
  // Coffre ouvert
  else {
    deverrouiller();
    chronoVerrouillage.stop();

    if (!estActif()) {
      activer(); 
      allumerSon();
      allumerLeds();
      allumerProjecteur();
    }
  }

}


// Le capot du coffre est ouvert ou fermé
bool Coffre::estOuvert() {
  return !btnOuverture.isPressed();
}

bool Coffre::estFerme() {
  return btnOuverture.isPressed();
}

// La galette qui actionne le coffre est présente ou absente
bool Coffre::declencheurPresent() {
  return btnDeclencheur.isPressed();
}

bool Coffre::declencheurAbsent() {
  return !btnDeclencheur.isPressed();
}


// Actionner le verrou electromagnetique
void Coffre::verrouiller() {
  digitalWrite(pinVerrouillage, LOW);
  deverrouillageActif = false;
}

// Arrêter le verrou electromagnetique
void Coffre::deverrouiller() {
  digitalWrite(pinVerrouillage, HIGH);
  deverrouillageActif = true;
}

// Indique si le verrou est verrouillé (true) ou déverrouillé (false)
bool Coffre::estVerrouille() {
  return !deverrouillageActif;
}

// Indique si le verrou est deverrouillé (true) ou verrouillé (false)
bool Coffre::estDeverrouille() {
  return deverrouillageActif;
}


// Allumer les leds
void Coffre::allumerLeds() {
  digitalWrite(pinLeds, HIGH);
}

// Éteindre les leds
void Coffre::eteindreLeds() {
  digitalWrite(pinLeds, LOW);
}


// Allumer le projecteur
void Coffre::allumerProjecteur() {
  colorerProjecteur(couleur);
}

// Éteindre le projecteur
void Coffre::eteindreProjecteur() {
  colorerProjecteur(Color(0, 0, 0));
}

// Définir la couleur du projecteur
void Coffre::colorerProjecteur(Color c) {
  DmxSimple.write(DmxAdresse, c.red);
  DmxSimple.write(DmxAdresse + 1, c.green);
  DmxSimple.write(DmxAdresse + 2, c.blue);
  DmxSimple.write(DmxAdresse + 3, 0);
  DmxSimple.write(DmxAdresse + 4, 0);
  DmxSimple.write(DmxAdresse + 5, 0);
}



// Activer le coffre
void Coffre::activer() {
  allumerProjecteur();
  actif = true;
}

// Désactiver le coffre
void Coffre::desactiver() {
  actif = false;
}

// Indique si  le coffre est actif ou non
bool Coffre::estActif() {
  return actif;
}


// Allumer le son
void Coffre::allumerSon() {
  digitalWrite(pinSon, HIGH);
}

// Couper le son
void Coffre::couperSon() {
  digitalWrite(pinSon, LOW);
}

